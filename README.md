# ER1191
Ce dossier fournit les programmes permettant de reproduire la méthodologie et les figures de l'Études et résultats n° 1191 de la DREES : « Assurance chômage, prestations sociales et prélèvements obligatoires atténuent de 70% les variations annuelles de niveau de vie des personnes d'âge actif », avril 2021.

Un ensemble de 4 programmes est mis à disposition :
- Le programme 0_initialisation.R définit l’environnement, les chemins et charge les fonctions nécessaires à l'analyse. Ce code doit tourner au début de chaque nouvelle session R. L’utilisateur remplacera la localisation de l’environnement de travail définie dans la commande « setwd » par son propre chemin d’accès aux programmes et « path » par le chemin d’accès aux tables de l’EDP.
- Le programme 1_donnees.R construit la base de données, qui est enregistrée dans le dossier « data » créé là où les programmes se situent. Ce programme est constitué de trois parties indépendantes. Des versions intermédiaires de la base sont enregistrées dans le dossier « data » entre deux parties. Une fois que les bases de données sont enregistrées, tous les autres programmes peuvent tourner indépendamment les uns des autres.
- Le programme 2_resultats.R exporte les données nécessaires à la création des figures de l’ER. Ces informations sont enregistrées au format .xls dans le dossier « resultats » créé là où les codes sont situés.
- Le programme 3_robustesse.R reproduit les graphiques 1, 2 et 3 de l'ER en modifiant certaines hypothèses (variabilité conditionnelle au rang ainsi qu'à la génération et à l'année, absence de correction du cycle de vie, classement selon le revenu disponible moyen non corrigé du cycle de vie) et pour des sous-populations d'intérêt (ménages de taille constante sur la période d'étude, ménages de taille variable, individus observés chaque année de 2011 à 2016). 
- Le programme fonctions.R regroupe les fonctions utilisées par les autres programmes.


Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/publications/etudes-et-resultats/assurance-chomage-prestations-sociales-et-prelevements

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Échantillon Démographique Permanent (INSEE), version de juin 2019. Traitements : Drees. Ces données sont disponibles en accès sécurisé via le CASD : https://www.casd.eu/en/source/permanent-demographic-sample/
Lien vers la documentation de l’EDP : https://utiledp.site.ined.fr/

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 4.0.2, le 15/04/2021.
